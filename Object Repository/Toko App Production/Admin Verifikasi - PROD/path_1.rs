<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path_1</name>
   <tag></tag>
   <elementGuidId>140c7f26-b58a-48ce-b77a-eb854ce9cb3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;root&quot;)/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-bsco3b r-13awgt0 r-1tsuqlv r-1udh08x&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-18u37iz r-1pi2tsx&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-tiot2n r-13l2t4g r-5kkj8d r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-14lw9ot r-qklmqi r-tiot2n r-1guathk r-1yzf0co&quot;]/div[@class=&quot;r-1awozwy r-14lw9ot r-tiot2n r-1jkafct r-rs99b7 r-1loqt21 r-18u37iz r-1wtj0ep r-ymttw5 r-5njf8e r-1otgn73 r-1i6wzkk r-lrvibr css-1dbjc4n&quot;]/svg[@class=&quot;MuiSvgIcon-root jss237&quot;]/path[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg.MuiSvgIcon-root.jss237 > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>97ead721-d77b-45da-935d-6fa869c7f8b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M11 18h2c.55 0 1-.45 1-1s-.45-1-1-1h-2c-.55 0-1 .45-1 1s.45 1 1 1zM3 7c0 .55.45 1 1 1h16c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1zm4 6h10c.55 0 1-.45 1-1s-.45-1-1-1H7c-.55 0-1 .45-1 1s.45 1 1 1z</value>
      <webElementGuid>13078944-293f-4e22-b0d6-0214a73c2cbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-bsco3b r-13awgt0 r-1tsuqlv r-1udh08x&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-18u37iz r-1pi2tsx&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-tiot2n r-13l2t4g r-5kkj8d r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-14lw9ot r-qklmqi r-tiot2n r-1guathk r-1yzf0co&quot;]/div[@class=&quot;r-1awozwy r-14lw9ot r-tiot2n r-1jkafct r-rs99b7 r-1loqt21 r-18u37iz r-1wtj0ep r-ymttw5 r-5njf8e r-1otgn73 r-1i6wzkk r-lrvibr css-1dbjc4n&quot;]/svg[@class=&quot;MuiSvgIcon-root jss237&quot;]/path[1]</value>
      <webElementGuid>24a0c736-f083-487c-b670-bd23cb888974</webElementGuid>
   </webElementProperties>
</WebElementEntity>
