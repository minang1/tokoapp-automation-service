import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import io.appium.java_client.android.nativekey.AndroidKey as AndroidKey
import io.appium.java_client.android.nativekey.KeyEvent as KeyEvent
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.google.common.collect.ImmutableMap as ImmutableMap

Mobile.sendKeys(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Login Page - Input No HP (Attrib)'), '0895353792793')

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Login Page - Button Login (Attrib)'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Login Method Page - Login via Password (Atrrib)'), 
    0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.sendKeys(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Login Method Page - Input Password (Attrib)'), 
    'A123456@!')

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Login Method Page - Button Login'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Homepage - Close Popup (Attrib) as Registered User'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Homepage - Mulai Order Baru (Attrib)'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Order Baru - Pilih Brand - Search Product (Attrib)'), 
    0)

Mobile.setText(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Order Baru - Pilih Brand - Input Search Product (Attrib)'), 
    '20250106000000', 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Order Baru - Product List - Add Qty (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Order Baru - Product List - Button Beli (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Review Order 1 - Pilih Agen (Attrib)'), 0)

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
int startX = device_Width / 2

'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
int endX = startX

'Storing the startY value'
int startY = device_Height * 0.30

'Storing the endY value'
int endY = device_Height * 0.70

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

/*Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Catatan'), 0)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('Toko App Production/Toko App - PROD/Order - Catatan'), 'smoke test - order 1 brand', 0)*/
int timeout = 10

def inputField = findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Review Order 1 - Header Catatan (Attrib)')

Mobile.tap(inputField, timeout)

Mobile.sendKeys(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Review Order 1 - Edit Catatan (Attrib)'), 
    'DEMO SMOKE TEST - ORDER 1 BRAND')

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Review Order 1 - Button Lanjutkan (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Review Order 2 - Button Konfirmasi (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Review Order 2 - Close Popup (Attrib)'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Ringkasan Order - Order Baru (Attrib)'), 0)

