import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import io.appium.java_client.android.nativekey.AndroidKey as AndroidKey
import io.appium.java_client.android.nativekey.KeyEvent as KeyEvent

Mobile.startExistingApplication('com.dbo.newdboindonesiahd', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Login Page - Masuk sebagai Tamu'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Redeem/Beranda  - Untung'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Redeem/Beranda  - Untung'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Potongan Harga (2)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Potongan Harga - 50jt (2)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Tukar Poin'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Close Prompt Login as Guest'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Potongan Harga - Back'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Beranda'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Beranda - Mulai Order Baru'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Rucika'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Lem'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Ruglue'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Add QTY (Ruglue) (Update Informasi Produk)'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Close Prompt Login as Guest'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Back'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Akun Saya'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Akun Saya - Klik Login (Guest)'), 0)

