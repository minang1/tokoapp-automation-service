import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\DBO-Minang\\Downloads\\toko.apk', true)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Update Codepush'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Homepage (as Guest) - Slide 1 (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Homepage (as Guest) - Slide 2 (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Homepage (as Guest) - Slide 3 (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Homepage (as Guest) - Slide 4 (Attrib)'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Homepage (as Guest) - Untung (Attrib)'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Untung (as Guest) - Slide 1 (Attrib)'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Untung (as Guest) - Slide 2 (Attrib)'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Untung (as Guest) - Slide 3 (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Homepage (as Guest) - Akun Saya (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Akun Saya (as Guest) - Slide 1 (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Akun Saya (as Guest) - Slide 2 (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Product Tour - Akun Saya (as Guest) - Slide 3 (Attrib)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - Prod (Rilis Juli)/Homepage (as Guest) - Login'), 0)

