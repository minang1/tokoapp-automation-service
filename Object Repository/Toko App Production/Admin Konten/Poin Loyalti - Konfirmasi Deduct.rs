<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Poin Loyalti - Konfirmasi Deduct</name>
   <tag></tag>
   <elementGuidId>ff52629e-9535-4877-a387-aac56f016cde</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Anda akan mengurangi 1 poin pada toko demo gamifikasi1'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.css-1dbjc4n.r-18u37iz.r-1wtj0ep.r-13qz1uu > button.MuiButtonBase-root.MuiButton-root.MuiButton-text.jss121.jss125.MuiButton-disableElevation > span.MuiButton-label > div.css-901oao</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>427ccc9a-35e7-4421-86df-d7a9157d2e0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>e1b6d489-8036-4798-8947-25f0b7fadfd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-901oao</value>
      <webElementGuid>5555c240-c1a5-4af7-a0ca-33405ce88eed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ya, lanjutkan</value>
      <webElementGuid>6ae01a9f-995a-4b3f-aac2-dba340811e2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;MuiDialog-root&quot;]/div[@class=&quot;MuiDialog-container MuiDialog-scrollPaper&quot;]/div[@class=&quot;MuiPaper-root MuiDialog-paper MuiDialog-paperScrollPaper MuiDialog-paperWidthSm MuiPaper-elevation24 MuiPaper-rounded&quot;]/div[@class=&quot;MuiDialogContent-root&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-1wzrnnt&quot;]/div[@class=&quot;css-1dbjc4n r-18u37iz r-1wtj0ep r-13qz1uu&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text jss121 jss125 MuiButton-disableElevation&quot;]/span[@class=&quot;MuiButton-label&quot;]/div[@class=&quot;css-901oao&quot;]</value>
      <webElementGuid>99fa66cc-53bf-4c1a-8eb7-a8f8001c8aca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Anda akan mengurangi 1 poin pada toko demo gamifikasi1'])[1]/following::div[2]</value>
      <webElementGuid>094c22df-03e4-421a-851a-e2ce21bb0d88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[11]</value>
      <webElementGuid>e77f91e6-5af8-439e-92f7-06f3157b4997</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak, batalkan'])[1]/preceding::div[1]</value>
      <webElementGuid>43f63bca-b8b1-44ad-9876-cb217e85a4c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ya, lanjutkan']/parent::*</value>
      <webElementGuid>4cd9717e-d417-4d19-89b0-d1c5981ce444</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div[2]/button/span/div</value>
      <webElementGuid>7118e5b5-1300-4819-8718-3a4c0b30c6e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Ya, lanjutkan' or . = 'Ya, lanjutkan')]</value>
      <webElementGuid>5ed35f57-bf53-4261-a990-4b41515b1055</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
