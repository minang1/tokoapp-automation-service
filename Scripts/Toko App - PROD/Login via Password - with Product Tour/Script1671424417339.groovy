import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.startApplication('C:\\Users\\DBO-Minang\\Downloads\\98.apk', false)

Mobile.setText(findTestObject('Toko App Production/Toko App - PROD/Login Page - Nomor Telepon (Update 4-Jan)'), '0895353792793', 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Login Page - Login'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Login Page - Tetap Gunakan Password'), 0)

Mobile.setText(findTestObject('Toko App Production/Toko App - PROD/Login Page - Masukkan Password'), 'A123456@!', 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Login Page - Log in Pswd btn'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Mulai Tour Sekarang'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 1 of 4'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 2 of 4'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 3 of 4'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 4 of 4'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Akun Saya'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Beranda - Mulai Order Baru'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Rucika'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Lem'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Ruglue'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Add QTY (Ruglue - Update 19 Des)'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Beli'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Order - Lanjutkan 1 of 1'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Review Order - Back'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Home'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Simpan Draf - No'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Redeem/Beranda  - Untung'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - DBO Untung - Lanjutkan 1 of 3'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - DBO Untung - Lanjutkan 2 of 3'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - DBO Untung - Lanjutkan 3 of 3'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Akun Saya'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Akun Saya'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Akun Saya - Lanjutkan 1 of 3'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Akun Saya - Lanjutkan 2 of 3'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Akun Saya - Lanjutkan 3 of 3'), 0)

