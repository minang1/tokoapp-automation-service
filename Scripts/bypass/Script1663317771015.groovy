import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://app.dbo.id/dashboard')

WebUI.setText(findTestObject('Object Repository/Toko App Production/bypass/Page_DBO - login/input_NIK atau Email_NIK'), '8102021')

WebUI.setEncryptedText(findTestObject('Object Repository/Toko App Production/bypass/Page_DBO - login/input_Password_password'), 'iFGeFYmXIrUhQZHvW7P22w==')

WebUI.click(findTestObject('Object Repository/Toko App Production/bypass/Page_DBO - login/button_Login'))

WebUI.click(findTestObject('Object Repository/Toko App Production/bypass/Page_DBO - Dashboard/a_Signin'))

WebUI.click(findTestObject('Toko App Production/bypass/Page_DBO - postbypass/span_HDS'))

WebUI.setText(findTestObject('Toko App Production/bypass/Page_DBO - postbypass/input__select2-search__field'), 'Monitoring Systems')

WebUI.sendKeys(findTestObject('Toko App Production/bypass/Page_DBO - postbypass/input__select2-search__field'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Toko App Production/bypass/Page_DBO - postbypass/button_Sign in'))

WebUI.closeBrowser()

