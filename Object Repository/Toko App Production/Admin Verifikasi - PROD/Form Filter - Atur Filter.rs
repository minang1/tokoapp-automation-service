<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Form Filter - Atur Filter</name>
   <tag></tag>
   <elementGuidId>376f4daf-6f5e-4a1f-9737-a65f2734d1e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.MuiDialogActions-root.MuiDialogActions-spacing > div.css-1dbjc4n > button.MuiButtonBase-root.MuiButton-root.MuiButton-text.jss120.jss122.MuiButton-disableElevation > span.MuiButton-label > div.css-901oao</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5b9a0a53-04ef-4a76-8bb1-ffe1c37a87fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>a023f472-c04e-46f3-9745-749121b1c085</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-901oao</value>
      <webElementGuid>c72bdbd7-1596-49bb-8af3-0bc59e7ba121</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Atur Filter</value>
      <webElementGuid>a736f3af-7516-4c55-8aa3-26587fc0a4dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;MuiDialog-root&quot;]/div[@class=&quot;MuiDialog-container MuiDialog-scrollPaper&quot;]/div[@class=&quot;MuiPaper-root MuiDialog-paper MuiDialog-paperScrollPaper MuiDialog-paperWidthSm MuiPaper-elevation24 MuiPaper-rounded&quot;]/div[@class=&quot;MuiDialogActions-root MuiDialogActions-spacing&quot;]/div[@class=&quot;css-1dbjc4n&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text jss120 jss122 MuiButton-disableElevation&quot;]/span[@class=&quot;MuiButton-label&quot;]/div[@class=&quot;css-901oao&quot;]</value>
      <webElementGuid>144bc0ed-5165-4c88-bd5d-c856734f00f6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset'])[1]/following::div[2]</value>
      <webElementGuid>029d0bd2-07d6-4f8d-8774-168d844f67d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sampai'])[2]/following::div[8]</value>
      <webElementGuid>6cc720a4-69c7-49a1-998a-63e7d6921ba7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Atur Filter']/parent::*</value>
      <webElementGuid>cf661270-22b8-4973-ab31-50c01c3ecea9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div[2]/button/span/div</value>
      <webElementGuid>98a729e3-dfaa-488b-8b57-f5e485f7409e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Atur Filter' or . = 'Atur Filter')]</value>
      <webElementGuid>5606a3dc-7ba8-4fa5-8ffd-171046840d3d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
