<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Kata Sandi_MuiInputBase-input MuiOutl_9c0fb1</name>
   <tag></tag>
   <elementGuidId>33c671a4-c448-4a4a-a59c-aaf1f49f8415</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiInputBase-root.MuiOutlinedInput-root.jss17.Mui-focused.Mui-focused.jss18.MuiInputBase-formControl > input.MuiInputBase-input.MuiOutlinedInput-input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7ff12422-b39f-4b8b-8580-3c91d70a8714</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>832c3a29-f518-43ea-80ca-f23a2d43aa68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>6d124d62-9bce-49e8-88df-d7b159bc004a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>394f68d9-7571-4240-a458-e406247cbc14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-cy</name>
      <type>Main</type>
      <value>password-input</value>
      <webElementGuid>7cfb20eb-e86b-4cd8-8c63-97f78e0a1d64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input</value>
      <webElementGuid>8a6b5e15-3e6e-48ba-9351-a0c6e5bf7540</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-focusvisible-polyfill</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>46df5fe2-bd59-4e46-8b35-4d8a7220e468</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-13awgt0 r-1777fci&quot;]/div[@class=&quot;css-1dbjc4n r-117bsoe r-1ovo9ad&quot;]/div[@class=&quot;css-1dbjc4n&quot;]/div[@class=&quot;MuiFormControl-root MuiTextField-root&quot;]/div[@class=&quot;MuiInputBase-root MuiOutlinedInput-root jss17 Mui-focused Mui-focused jss18 MuiInputBase-formControl&quot;]/input[@class=&quot;MuiInputBase-input MuiOutlinedInput-input&quot;]</value>
      <webElementGuid>cf736b06-82ef-4f53-8204-36a0b083582d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='']</value>
      <webElementGuid>5b44194b-07b6-40b8-b694-254d23605a2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[3]/div[2]/div/div/input</value>
      <webElementGuid>8bcce54e-8eca-4c4c-a5a5-bb8bbbad09e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div/input</value>
      <webElementGuid>f95463ba-0547-416f-95fb-9251b01da02f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password']</value>
      <webElementGuid>5104557d-e270-40bc-bcbf-52f6ba61a6c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
