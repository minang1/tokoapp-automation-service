import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Rucika'), 0)

Mobile.tap(findTestObject('Object Repository/Toko App Production/Toko App - PROD/Order - Pilih Lem'), 0)

Mobile.tap(findTestObject('Object Repository/Toko App Production/Toko App - PROD/Order - Pilih Ruglue'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Add QTY (Ruglue - Update 21 Des)'), 0)

Mobile.tap(findTestObject('Object Repository/Toko App Production/Toko App - PROD/Order - Beli'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Tambah Barang'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Pilih Kategori Brand'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Pilih Djabesmen'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Pilih Aksesoris'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Pilih Djabeskrup'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Add QTY (Djabeskrup) (Update Informasi Produk)'), 
    0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Beli'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Distributor Demo MU Era'), 0)

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
int startX = device_Width / 2

'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
int endX = startX

'Storing the startY value'
int startY = device_Height * 0.30

'Storing the endY value'
int endY = device_Height * 0.70

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

int timeout = 10

def inputField = findTestObject('Toko App Production/Toko App - PROD/Order2 - Catatan (1)')

Mobile.tap(inputField, timeout)

Mobile.sendKeys(inputField, 'smoke test - order 2 brand')

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Header Catatan'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Next Page'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Pilih Distributor Demo MU DBO'), 0)

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

Mobile.tap(inputField, timeout)

Mobile.sendKeys(inputField, 'smoke test - order 2 brand')

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Header Catatan'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Next Page'), 0)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Konfirmasi Order'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Close Popup2'), 0)

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order2 - Beranda'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Redeem/Beranda  - Untung'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

