<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Poin Loyalti - Konfirmasi topup</name>
   <tag></tag>
   <elementGuidId>cc361b27-5de9-4c4b-a0ac-4797d3a75737</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Anda akan menambah 1 poin kepada toko demo gamifikasi1'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.css-1dbjc4n.r-18u37iz.r-1wtj0ep.r-13qz1uu > button.MuiButtonBase-root.MuiButton-root.MuiButton-text.jss249.jss253.MuiButton-disableElevation > span.MuiButton-label > div.css-901oao</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9102236d-6fb2-496b-aef4-0918d7176e88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>e7b7943c-c128-4f22-86d1-05463a5c4ac3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-901oao</value>
      <webElementGuid>b260027c-258f-48d9-9ab2-5badc822f7e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ya, lanjutkan</value>
      <webElementGuid>9804c7e6-56bb-4065-8951-b2d599ddb1f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;MuiDialog-root&quot;]/div[@class=&quot;MuiDialog-container MuiDialog-scrollPaper&quot;]/div[@class=&quot;MuiPaper-root MuiDialog-paper MuiDialog-paperScrollPaper MuiDialog-paperWidthSm MuiPaper-elevation24 MuiPaper-rounded&quot;]/div[@class=&quot;MuiDialogContent-root&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-1wzrnnt&quot;]/div[@class=&quot;css-1dbjc4n r-18u37iz r-1wtj0ep r-13qz1uu&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text jss249 jss253 MuiButton-disableElevation&quot;]/span[@class=&quot;MuiButton-label&quot;]/div[@class=&quot;css-901oao&quot;]</value>
      <webElementGuid>ef831886-f333-4306-8461-df0d52f659b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Anda akan menambah 1 poin kepada toko demo gamifikasi1'])[1]/following::div[2]</value>
      <webElementGuid>a61e96fd-2277-4b8a-8ca5-3f7ecc06659b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[11]</value>
      <webElementGuid>6e5153b0-6910-48d0-a8dd-5863de2fe85a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak, batalkan'])[1]/preceding::div[1]</value>
      <webElementGuid>09386642-9512-4064-b0e5-2f52cfa1beb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ya, lanjutkan']/parent::*</value>
      <webElementGuid>a171329b-4c79-4c65-b9d0-fe87f5894397</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div[2]/button/span/div</value>
      <webElementGuid>4b277eb1-ba52-4b33-b493-f9b52d3b33dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Ya, lanjutkan' or . = 'Ya, lanjutkan')]</value>
      <webElementGuid>613284ad-241f-4bb3-b032-d1807ed5e72b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
