<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>0a0cc621-ea49-4b70-8d2b-4ff980b96dcf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;root&quot;)/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-bsco3b r-13awgt0 r-1tsuqlv r-1udh08x&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-150rngu r-eqz5dr r-16y2uox r-1wbh5a2 r-11yh6sk r-1rnoaur r-1sncvnh&quot;]/div[@class=&quot;css-1dbjc4n r-bsco3b r-13awgt0 r-11yd5a9 r-1h4fu65&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-18u37iz r-1wtj0ep r-1l7z4oj&quot;]/div[@class=&quot;r-1awozwy r-14lw9ot r-tiot2n r-1jkafct r-rs99b7 r-1loqt21 r-18u37iz r-1wtj0ep r-ymttw5 r-5njf8e r-1otgn73 r-1i6wzkk r-lrvibr r-l0gwng css-1dbjc4n&quot;]/svg[@class=&quot;MuiSvgIcon-root jss237&quot;]/path[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg.MuiSvgIcon-root.jss237 > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>d2869ce8-8548-4ed6-86cf-8d5746f3c0ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M11 18h2c.55 0 1-.45 1-1s-.45-1-1-1h-2c-.55 0-1 .45-1 1s.45 1 1 1zM3 7c0 .55.45 1 1 1h16c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1zm4 6h10c.55 0 1-.45 1-1s-.45-1-1-1H7c-.55 0-1 .45-1 1s.45 1 1 1z</value>
      <webElementGuid>72f30e22-92b6-4ca7-8ab3-b10732068eb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-bsco3b r-13awgt0 r-1tsuqlv r-1udh08x&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0&quot;]/div[@class=&quot;css-1dbjc4n r-150rngu r-eqz5dr r-16y2uox r-1wbh5a2 r-11yh6sk r-1rnoaur r-1sncvnh&quot;]/div[@class=&quot;css-1dbjc4n r-bsco3b r-13awgt0 r-11yd5a9 r-1h4fu65&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-18u37iz r-1wtj0ep r-1l7z4oj&quot;]/div[@class=&quot;r-1awozwy r-14lw9ot r-tiot2n r-1jkafct r-rs99b7 r-1loqt21 r-18u37iz r-1wtj0ep r-ymttw5 r-5njf8e r-1otgn73 r-1i6wzkk r-lrvibr r-l0gwng css-1dbjc4n&quot;]/svg[@class=&quot;MuiSvgIcon-root jss237&quot;]/path[1]</value>
      <webElementGuid>75fa1b5e-1075-4023-9bfa-d18247771eba</webElementGuid>
   </webElementProperties>
</WebElementEntity>
