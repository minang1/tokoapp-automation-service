import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import io.appium.java_client.android.nativekey.AndroidKey as AndroidKey
import io.appium.java_client.android.nativekey.KeyEvent as KeyEvent
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory

Mobile.startApplication('C:\\Users\\DBO-Minang\\Downloads\\98.apk', false)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Login Page - Masuk sebagai Tamu'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Mulai Tour Sekarang'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 1 of 4'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 2 of 4'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 3 of 4'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - Homepage - Lanjutkan 4 of 4'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Toko App Production/Toko App - PROD/Beranda - Mulai Order Baru'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.sendKeys(findTestObject('Toko App Production/Toko App - PROD/Beranda - Cari Produk'), 'ruglue\n', FailureHandling.STOP_ON_FAILURE)

not_run: AndroidDriver<?> driver = MobileDriverFactory.getDriver()

not_run: driver.pressKey(new KeyEvent(AndroidKey.SEARCH))

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Redeem/Beranda  - Untung'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Redeem/Beranda  - Untung'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - DBO Untung - Lanjutkan 1 of 3'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - DBO Untung - Lanjutkan 2 of 3'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Product Tour - DBO Untung - Lanjutkan 3 of 3'), 
    0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Redeem/Beranda  - Untung'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Potongan Harga (2)'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Potongan Harga - 50jt (2)'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Tukar Poin'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Close Prompt Login as Guest'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/DBO Untung - Potongan Harga - Back'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Beranda'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Beranda - Mulai Order Baru'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Rucika'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Lem'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Pilih Ruglue'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Add QTY (Ruglue - Update 21 Des)'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Close Prompt Login as Guest'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Back'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Order - Home'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Akun Saya'), 0)

not_run: Mobile.tap(findTestObject('Toko App Production/Toko App - PROD/Akun Saya - Klik Login (Guest)'), 0)

