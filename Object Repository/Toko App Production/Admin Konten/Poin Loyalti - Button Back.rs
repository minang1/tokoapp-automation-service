<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Poin Loyalti - Button Back</name>
   <tag></tag>
   <elementGuidId>e0bfcc3c-f43a-4bb0-9fd4-1786d8ed43b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#root > div > div > div > div.css-1dbjc4n.r-1awozwy.r-18u37iz.r-1wtj0ep.r-13qz1uu > button > span.MuiIconButton-label > svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;root&quot;]/div/div/div/div[1]/button/span[1]/svg</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div/div/div/div[1]/button/span[1]/svg/path</value>
      <webElementGuid>969f5e03-3e7c-4890-a690-6541fbf26ece</webElementGuid>
   </webElementProperties>
</WebElementEntity>
