<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Poin Loyalti - Pilih Brand</name>
   <tag></tag>
   <elementGuidId>64c6b7d5-f097-48d4-b5dc-8e9559a1f9b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[7]/div/div[2]/div/div/button/span/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.css-1dbjc4n.r-1awozwy.r-18u37iz.r-xd6kpl > div.css-1dbjc4n.r-1awozwy.r-18u37iz > div.css-1dbjc4n.r-1pyaxff.r-dqa1tq > div.css-1dbjc4n.r-18u37iz.r-2yi16 > button.MuiButtonBase-root.MuiButton-root.jss526.MuiButton-text.jss521 > span.MuiButton-label.jss523 > div.css-1dbjc4n.r-13awgt0 > div.css-901oao.css-bfa6kz</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e8dfb9e9-c24b-491d-80e1-c61d2af16cdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>ebad2c0b-fc07-48ba-98e8-0807c49dad2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-901oao css-bfa6kz</value>
      <webElementGuid>32ad8672-e806-4146-9f31-235552976db1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih Brand</value>
      <webElementGuid>41e4d84a-341f-4d89-a927-b0335e3a4000</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-hfja1k&quot;]/div[@class=&quot;css-1dbjc4n r-4remjh r-qklmqi r-1peese0 r-qn3fzs&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-18u37iz r-xd6kpl&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-18u37iz&quot;]/div[@class=&quot;css-1dbjc4n r-1pyaxff r-dqa1tq&quot;]/div[@class=&quot;css-1dbjc4n r-18u37iz r-2yi16&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root jss526 MuiButton-text jss521&quot;]/span[@class=&quot;MuiButton-label jss523&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0&quot;]/div[@class=&quot;css-901oao css-bfa6kz&quot;]</value>
      <webElementGuid>6604bea8-78e2-4a03-8c49-3ad7242cba8d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[7]/div/div[2]/div/div/button/span/div/div</value>
      <webElementGuid>2aefc120-0793-4fca-8b9b-079d8511d301</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Poin'])[1]/following::div[5]</value>
      <webElementGuid>fed379b1-f672-4ec8-bff4-d202c61799b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aksi'])[1]/following::div[11]</value>
      <webElementGuid>4cfeb4eb-6672-4ec8-b327-5dabbae461f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih tipe'])[1]/preceding::div[1]</value>
      <webElementGuid>a50d3b38-24a6-43ef-b8ed-8ba09fa100fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cari'])[1]/preceding::div[7]</value>
      <webElementGuid>9c62ff5e-06bc-4a0a-bf61-4764f10d5185</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih Brand']/parent::*</value>
      <webElementGuid>e461126c-b484-4369-b3d9-f3d5437e75e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/button/span/div/div</value>
      <webElementGuid>252b9905-a864-4307-becb-342ea387150f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Pilih Brand' or . = 'Pilih Brand')]</value>
      <webElementGuid>617864c4-93fb-4720-bfb4-2910fda8dcb7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
